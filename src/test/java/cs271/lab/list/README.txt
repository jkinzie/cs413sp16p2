//Q; which of the two lists performs better as the size increases?
//A: The linked list performs better since it can handle additions much faster than a regular array.

//Q: TODO what happens if you use list.remove(77)?
//A: This will fail since 77 is an int and not an Integer. If you used an Integer,
//it would try to remove the element at index 77 which will cause
//an IndexOutOfBoundsException error to occur since the list not that large.


//See the results below for real runtime data:

testArrayListAccess took: 11 milli-seconds. with size 10
testLinkedListAddRemove took: 35 milli-seconds. with size 10
testLinkedListAccess took: 18 milli-seconds. with size 10
testArrayListAddRemove took: 32 milli-seconds. with size 10

testArrayListAccess took: 16 milli-seconds. with size 100
testLinkedListAddRemove took: 34 milli-seconds. with size 100
testLinkedListAccess took: 38 milli-seconds. with size 100
testArrayListAddRemove took: 54 milli-seconds. with size 100

testArrayListAccess took: 13 milli-seconds. with size 1000
testLinkedListAddRemove took: 34 milli-seconds. with size 1000
testLinkedListAccess took: 383 milli-seconds. with size 1000
testArrayListAddRemove took: 282 milli-seconds. with size 1000

testArrayListAccess took: 14 milli-seconds. with size 10000
testLinkedListAddRemove took: 36 milli-seconds. with size 10000
testLinkedListAccess took: 5171 milli-seconds. with size 10000
testArrayListAddRemove took: 1788 milli-seconds. with size 10000

It is hard to see this without a graph...so I made a graph!

At the lower numbers of elements,the execution time is approximately
the same for all tests. As the number of elements increase, the time
to execute increases exponentially. The arrayLists faired slightly better,
however, not by much. This is likely due to arrayList not needing to fully
resize the array.



